import React from 'react';
import ContactList from './views/ContactList';
import { Route, BrowserRouter as Router } from 'react-router-dom';
import ContactForm from './views/ContactForm';
import { ContactListProvider } from './context/ContactListProvider';
import { ContactFormProvider } from './context/ContactFormProvider';

const App = () => {

  return (
    <>
      <Router>
        <ContactListProvider>
          <Route path='/' exact component={ContactList} />

          <ContactFormProvider>
            <Route path='/new' component={ContactForm} />
            <Route path='/contact/:id' component={ContactForm} />
          </ContactFormProvider>
          
        </ContactListProvider>

      </Router>
    </>
  )
}

export default App;
