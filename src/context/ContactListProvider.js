import React, { createContext, useEffect, useReducer } from 'react';
import { ContactsService } from '../services/ContactsService';

export const ContactListStateContext = createContext();

export const ContactListDispatchContext = createContext();

export const ContactListProvider = (props) => {

    const [state, dispatch] = useReducer((contactsState, { type, payload }) => {

        switch (type) {

            case 'load':

                return payload

            case 'upsert':

                const { firstName, lastName, id } = payload;

                const fullName = `${firstName} ${lastName}`;

                const item = contactsState.find(i => i.id === id);

                if (item) {

                    item.fullName = fullName;
                }
                else {

                    contactsState.push({ id, fullName });
                }

                return contactsState;

            default:

                return contactsState;
        }

    }, [])


    useEffect(() => {

        (async () => {

            const payload = await ContactsService.fetchContactList();

            dispatch({ type: 'load', payload });

        })();

    }, []);

    return (
        <ContactListStateContext.Provider value={state}>
            <ContactListDispatchContext.Provider value={dispatch}>
                {props.children}
            </ContactListDispatchContext.Provider>
        </ContactListStateContext.Provider>
    )
} 