import React, { createContext, useReducer } from "react"

export const ContactFormStateContext = createContext();
export const ContactFormDispatchContext = createContext();

const initialState = {

    id: null,
    firstName: '',
    lastName: '',
    emailAddresses: []
}

export const ContactFormProvider = (props) => {

    const reducer = (person, { type, payload }) => {

        function changeEmailAddressField() {

            const { target, idx } = payload;

            const item = person.emailAddresses[idx];

            if (!item) return;

            const { name, value } = target;

            item[name] = value;

            return { ...person };
        }

        switch (type) {

            case 'empty':

                return initialState;

            case 'load':

                return { ...person, ...payload }

            case 'field-change':

                const { name, value } = payload;

                person[name] = value;

                return { ...person, ...{ [name]: value } };

            case 'email-address-change':

                return changeEmailAddressField();

            case 'email-address-add':

                person.emailAddresses.push({ emailAddress: '', type: '' });

                return { ...person };

            case 'email-address-remove':

                const { idx } = payload;

                person.emailAddresses.splice(idx, 1);

                return { ...person };

            default:

                return person;
        }
    }

    const [state, dispatch] = useReducer(reducer, initialState);

    return (
        <ContactFormStateContext.Provider value={state}>
            <ContactFormDispatchContext.Provider value={dispatch}>
                {props.children}
            </ContactFormDispatchContext.Provider>
        </ContactFormStateContext.Provider>
    )
}