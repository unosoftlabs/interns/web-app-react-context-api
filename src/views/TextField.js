import React, { useContext } from 'react';
import { Form } from 'semantic-ui-react';
import { ContactFormDispatchContext } from '../context/ContactFormProvider';

export const TextField = ({ name, label, value }) => {

    const formDispatch = useContext(ContactFormDispatchContext);

    return (

        <Form.Input

            name={name}
            label={label}
            value={value}
            onChange={(e, payload) => formDispatch({ type: 'field-change', payload })}

        />
    )
}