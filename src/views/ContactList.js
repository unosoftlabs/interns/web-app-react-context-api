import React, { useContext } from 'react';
import { Button, Icon, List } from 'semantic-ui-react';
import { ContactListStateContext } from '../context/ContactListProvider';
import { Link } from 'react-router-dom';

const ContactList = () => {

    const contacts = useContext(ContactListStateContext);

    return (
        <>
            <Link to='/new'>
                <Button icon>
                    <Icon name="add" />
                </Button>
            </Link>
            <h1>Contacts list</h1>
            <List divided size="massive">
                
                {contacts.map(contact => {

                    return (
                        <List.Item key={contact.id}>
                            <Link to={`/contact/${contact.id}`}>
                                <List.Header>{contact.fullName}</List.Header>
                            </Link>
                        </List.Item>
                    );
                })}
            </List>
        </>
    )

}

export default ContactList;