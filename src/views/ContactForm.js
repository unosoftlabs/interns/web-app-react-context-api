import React, { useContext, useEffect, useState } from 'react';
import { Button, Icon, Header, Form, Message, Grid } from 'semantic-ui-react';
import { Link } from 'react-router-dom';
import { ContactsService } from '../services/ContactsService';
import { ContactFormDispatchContext, ContactFormStateContext } from '../context/ContactFormProvider';
import { ContactListDispatchContext } from '../context/ContactListProvider';
import { TextField } from './TextField';

const ContactForm = (props) => {

    const [showSaved, setSaved] = useState(false);

    const formState = useContext(ContactFormStateContext);

    const formDispatch = useContext(ContactFormDispatchContext);

    const listDispatch = useContext(ContactListDispatchContext);

    useEffect(() => {

        const { id } = props.match.params;

        if (!id) {

            return formDispatch({ type: 'empty' });
        };

        (async () => {

            const payload = await ContactsService.fetchContact(id);

            formDispatch({ type: 'load', payload });

        })();

    }, [props.match.params, formDispatch]);

    const savePerson = async () => {

        const payload = await ContactsService.saveContact(formState);

        formDispatch({ type: 'load', payload });

        listDispatch({ type: 'upsert', payload });

        setSaved(true);

        setTimeout(() => { setSaved(false); }, 2000);
    }

    const { id, firstName, emailAddresses, lastName } = formState;

    const emailAddressTypeOptions = [

        { key: '', value: '', text: '' },
        { key: 'home', value: 'home', text: 'Home' },
        { key: 'work', value: 'work', text: 'Work' },
        { key: 'other', value: 'other', text: 'Other' }
    ];

    return (
        <>
            <Link to='/'>
                <Button icon>
                    <Icon name='left arrow' />
                </Button>
            </Link>
            <Header>Contact Id {id || '(none)'}</Header>

            <Form onSubmit={savePerson}>

                <TextField

                    name='firstName'
                    label='First name'
                    value={firstName}
                />

                <TextField

                    name='lastName'
                    label='Last name'
                    value={lastName}

                />

                <Form.Field>
                    <Form.Field>
                        Email addresses
                    </Form.Field>
                    <Button icon type='button' onClick={() => formDispatch({ type: 'email-address-add' })}>
                        <Icon name='plus' />
                    </Button>
                </Form.Field>

                <Grid columns={3}>

                    {emailAddresses.map((item, idx) => {

                        return (

                            <Grid.Row key={idx}>

                                <Grid.Column>

                                    <Form.Input

                                        name='emailAddress'
                                        placeholder='Email address'
                                        onChange={(e, target) => formDispatch({ type: 'email-address-change', payload: { target, idx } })}
                                        value={item.emailAddress}
                                    />

                                </Grid.Column>

                                <Grid.Column>

                                    <Form.Select

                                        name='type'
                                        placeholder='Type'
                                        value={item.type}
                                        onChange={(e, target) => formDispatch({ type: 'email-address-change', payload: { target, idx } })}
                                        options={emailAddressTypeOptions}
                                    />
                                </Grid.Column>

                                <Grid.Column>

                                    <Button icon type='button' onClick={() => formDispatch({ type: 'email-address-remove', payload: { idx } })}>
                                        <Icon name='minus' />
                                    </Button>
                                </Grid.Column>
                            </Grid.Row>
                        )

                    })}

                </Grid>

                <Button>Save</Button>
            </Form>

            {showSaved ? <Message>Saved</Message> : null}
        </>
    )
}

export default ContactForm;