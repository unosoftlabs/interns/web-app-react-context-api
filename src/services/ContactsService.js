const apiPath = 'http://localhost:3001';

export const ContactsService = {

    fetchContact: async id => {

        const response = await fetch(`${apiPath}/contact/${id}`);

        const payload = await response.json();

        return payload;
    },

    saveContact: async person => {

        const packet = {

            method: 'POST',
            headers: { 'content-type': 'application/json' },
            body: JSON.stringify(person)
        };

        const response = await fetch(apiPath + '/contact', packet);

        const payload = await response.json();

        return payload;
    },

    fetchContactList: async () => {

        const response = await fetch(apiPath + '/contacts');

        const data = await response.json();

        return data;
    }
}